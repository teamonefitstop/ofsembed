import React from 'react';

const Banner = ({ appName, token }) => {
  const img1= "https://www.onefitstop.com/wp-content/uploads/2020/10/gym-fitness-clubs-a1.png";
  if (token) {
    return null;
  }
  return (
    <div className="banner container-fluid">
      <div className="row bannerRow">
        <div className="col-6 img src1"></div>
        <div className="col-6 img src2"></div>
      </div>
    </div>
  );
};

export default Banner;
