
import ClassList from '../ClassList'
import React from 'react';
import api from '../../api';
import { connect } from 'react-redux';
import { CHANGE_TAB } from '../../constants/actionTypes';

const ClasessTab = props => {
  const clickHandler = ev => {
    ev.preventDefault();
    props.onTabClick('all', api.Class.getAll, api.Class.getAll());
  };
  return (
    <li className="nav-item">
      <a
        href=""
        className={props.tab === 'all' ? 'nav-link active' : 'nav-link'}
        onClick={clickHandler}>
        Classes
      </a>
    </li>

  );
};

const mapStateToProps = state => ({
  ...state.classList,
  tags: state.home.tags,
  token: state.common.token
});

const mapDispatchToProps = dispatch => ({
  onTabClick: (tab, pager, payload) => dispatch({ type: CHANGE_TAB, tab, pager, payload })
});

const MainView = props => {
  return (
    <div className="col-md-12">
      <div className="feed-toggle">
        <ul className="nav nav-pills outline-active">
          <ClasessTab tab={props.tab} onTabClick={props.onTabClick} />    

        </ul>
      </div>
      <div className="container text-center listBucket">
      <ClassList
        pager={props.pager}
        classList={props.classList}
        loading={props.loading}
        classListCount={props.classListCount}
        currentPage={props.currentPage} />
    </div>
    </div>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(MainView);
