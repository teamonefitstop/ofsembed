import Class from './Class';
import ListPagination from './ListPagination';
import React from 'react';

const ClassList = props => {
  if (!props.classList) {
    return (
      <div className="article-preview">Loading...</div>
    );
  }

  if (props.classList.length === 0) {
    return (
      <div className="article-preview">
        No articles are here... yet.
      </div>
    );
  }

  return (
    <div className="container">
         <div class="row hd">
          <div class="col-2">
            Session Date
            </div>
            <div class="col-2">
            Start Time
            </div>
            <div class="col-2">
            Duration
            </div>
            <div class="col-2">
           Wait List Reserved
            </div> <div class="col-2">
            Wait List Remaining
            </div> <div class="col-2">
           Wait List Capacity
          </div>
      </div>

      {
        props.classList.map(classObj => {
          return (
            
              <Class classObj={classObj} />
           
          );

        })
      }

      <ListPagination
        pager={props.pager}
        classListCount={props.classListCount}
        currentPage={props.currentPage} />
     
    </div >
  );
};

export default ClassList;
