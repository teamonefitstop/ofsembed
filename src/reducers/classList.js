import {

  HOME_PAGE_LOADED,
  HOME_PAGE_UNLOADED


} from '../constants/actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case HOME_PAGE_LOADED:
      return {
        ...state,
        pager: action.pager,
        tags: action.payload[0].tags,
        articles: action.payload[1].articles,
        articlesCount: action.payload[1].articlesCount,
        classList: action.payload[2].data,
        classListCount: action.payload[2].totalRecords,
        currentPage: 0,
        tab: action.tab
      };
    case HOME_PAGE_UNLOADED:
      return {};

    default:
      return state;
  }
};
