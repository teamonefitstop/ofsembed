import superagentPromise from 'superagent-promise';
import _superagent from 'superagent';

const superagent = superagentPromise(_superagent, global.Promise);

const API_ROOT = 'https://api.onefitstop.com/v2/';
const responseBody = res => res.body;

const classHeaders = req => {
  req.set('accessID', 'ds5fy8901xj430yx3424t3pccvxqm8:5j76kg4eoglcfds35sbzlzwg5w4pvk');
  req.set('siteID', 'Tm9hSHRXUHpDQTI2TkxGczJ5RmJaUT09');

}
const requests = {
  getWithHeader: url =>
    superagent.get(`${API_ROOT}${url}`).use(classHeaders).then(responseBody)

};
const Class = {
  getAll: () => requests.getWithHeader('site/sessions')
};

// const limit = (count, p) => `limit=${count}&offset=${p ? p * count : 0}`;
// const omitSlug = article => Object.assign({}, article, { slug: undefined })

export default {
  Class

};
